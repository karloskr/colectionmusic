<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GenderController;
use App\Http\Controllers\StyleController;
use App\Http\Controllers\LabelController;
use App\Http\Controllers\AlbumController;
use App\Http\Controllers\BandController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\TrackController;
use App\Http\Controllers\ArtistController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\ArtistTrackController;




Route::get('/', function () {
    return view('welcome');
});

Route::prefix('admin')->group(function () {
    Route::get('/', function () { return view('admin');});
    Route::get('/usuarios', function () { return view('usuarios');});
});

Route::resource('genders', GenderController::class)->names('genders');
Route::resource('genders.styles', StyleController::class)->shallow();
Route::resource('labels', LabelController::class)->names('labels');
Route::resource('countries', CountryController::class)->names('countries');
Route::post('/search-countries', [CountryController::class, 'fetch'])->name('search.fetch');
Route::resource('bands', BandController::class)->names('bands');
/* Route::post('/search-bands', [BandController::class, 'fetch'])->name('search.band'); */
Route::resource('bands.members', MemberController::class)->shallow();
Route::resource('bands.albums', AlbumController::class)->shallow();
Route::resource('albums.tracks', TrackController::class)->shallow();
Route::resource('tracks.artist_tracks', ArtistTrackController::class)->shallow();
Route::resource('artists', ArtistController::class)->names('artists');

