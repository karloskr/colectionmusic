@extends('layouts.main')

@section('title', 'Genders')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Genders</h1>
    <a href="{{-- {{route('genders.create')}} --}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#modalCreate"><i
            class="fas fa-user fa-sm text-white-50"></i>Create</a>
</div> 

<div class="row">
    @include('custom.message')
</div>

<!-- List genders -->
<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Gender</th>
        <th scope="col">Styles</th>
        <th scope="col"></th>
        <th scope="col"></th>
      </tr>
    </thead>
    <tbody>
     @foreach ($genders as $gender)
     <tr>
        <td scope="row">{{ $gender->id }}</td>
        <td scope="row"><a href="{{ route('genders.show', $gender->id) }}">{{ $gender->name }}</a></td>
        <td scope="row">
          <a href="{{ route('genders.styles.index', $gender)}}">{{ $gender->styles_count}}</a>
        </td>
        <td scope="row"><a href="{{ route('genders.edit', $gender) }}" class="btn btn-primary">
        <i class="fa fa-edit"></i>
        </a></td>
        <td scope="row">
        <button class="btn btn-warning btnEliminar" data-id="{{ $gender->id }}" data-toggle="modal" data-target="#modalEliminar"><i class="fa fa-trash"></i></button>
          <form action="{{ route('genders.destroy', $gender) }}" id="formEli_{{ $gender->id}}" method="POST">
            @csrf
           {{-- @method('DELETE') --}}
            <input type="hidden" name="id" value="{{ $gender->id }}">
            <input type="hidden" name="_method" value="delete">
            
          </form>
        </td>
      </tr>
     @endforeach
    </tbody>
  </table>
  <span>
    {{ $genders->links() }}
  </span>

<!-- Modals -->
@include('genders.modalCreate')
@include('genders.modalDelete')
<!-- EndModals -->

@endsection

@section('scripts')
      <script>

        var idEliminar = 0;

          $(document).ready(function(){
            @if($message = session('ErrorInsert'))
                $('#modalCreate').modal('show');
            @endif
            $(".btnEliminar").click(function(){
                idEliminar = $(this).data('id');
            });
            $(".btnModalEliminar").click(function(){
                $("#formEli_" + idEliminar).submit();
            });

          });
      </script>
  @endsection

