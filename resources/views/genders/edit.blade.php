@extends('layouts.main')

@section('title', 'Update Gender')

@section('content')
<br>
  <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h2>Edit Genders</h2>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{route('genders.update', $gender)}}">
                    @csrf
                    @method('PUT')
            
                    <div class="form-group">
                      <label for="name">Name Gender</label>
                      <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $gender->name )}}">
                      <span style="color: red">@error('name'){{$message}}@enderror</span>
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a type="button" class="btn btn-success" href="{{ route('genders.index') }}">Back</a>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

