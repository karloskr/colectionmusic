@extends('layouts.main')

@section('title', 'Show Gender')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Genders</h1>
    <a href="{{route('genders.index')}}" class="btn btn-success shadow-sm">Back</a>
</div> 

<div class="card">
    <h5 class="card-header">Show Gender</h5>
    <div class="card-body">
      <h3 class="card-text">{{ $gender->name }}</h3>
    </div>
  </div>
  <br>
  <a href="{{ route('genders.index') }}" class="float-left btn btn-success">Back</a>
@endsection