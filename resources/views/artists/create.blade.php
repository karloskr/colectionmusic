@extends('layouts.main')

@section('title', 'Create Artist')

@section('content')

<div class="card">
    <h5 class="card-header">Create Artist</h5>
    <div class="card-body">
        <form method="POST" action="{{route('artists.store')}}">
            @csrf

            <label for="exampleFormControlSelect1">Countries</label>
          <select class="form-control" id="countries" name="country_id">
              <option class="hidden">Countries</option>
              @foreach ($countries as $country)
                      <option value="{{ $country->id }}">
                        {{ $country->name }}</option>
                    @endforeach
          </select>
          @error('country_id')
            <p class="help is-danger" style="color: red">{{ $message }}</p>
            @enderror

            <div class="form-group">
              <label for="name">Name Artists</label>
              <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
              @error('name')
            <p class="help is-danger" style="color: red">{{ $message }}</p>
            @enderror
              <label for="profile">Profile</label>
          <textarea class="form-control" id="profile" name="profile" value="{{ old('profile') }}"></textarea>
          @error('profile')
            <p class="help is-danger" style="color: red">{{ $message }}</p>
            @enderror
              <label for="name">Birth</label>
              <input type="date" class="form-control" id="birth" name="day_birth" value="{{ old('day_birth') }}">
              @error('day_birth')
            <p class="help is-danger" style="color: red">{{ $message }}</p>
            @enderror
              <label for="name">Death</label>
              <input type="date" class="form-control" id="death" name="day_death" value="{{ old('day_death') }}">
              @error('day_death')
              <p class="help is-danger" style="color: red">{{ $message }}</p>
              @enderror
              
            </div>
          
          <div class="modal-footer">
            <a href="{{ route('artists.index') }}" class="btn btn-success">Back</a>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
    </div>
</div>
    
@endsection