@extends('layouts.main')

@section('title', 'Update Artist')

@section('content')
<br>
  <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h2>Edit Artist</h2>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{route('artists.update', $artist)}}">
                    @csrf
                    @method('PUT')
            
                    <div class="form-group">
                    <label for="exampleFormControlSelect1">Countries</label>
                    <select class="form-control" id="countries" name="country_id">
                        <option class="hidden" disabled>Countries</option>
                        @foreach ($countries as $country)
                          <option value="{{ $country->id }}"
                            @if ($country->id === $artist->country_id)
                                selected
                            @endif
                            >
                            {{ $country->name }}</option>
                        @endforeach
                    </select>

                      <label for="name">Name Artist</label>
                      <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $artist->name )}}">
                      <span style="color: red">@error('name'){{$message}}@enderror</span>

                      <label for="profile">Profile</label>
                  <textarea class="form-control" id="profile" name="profile" value="{{ old('profile') }}">{{ $artist->profile }}</textarea>

                      <label for="name">Birth</label>
                      <input type="date" class="form-control" id="birth" name="day_birth" value="{{ old('day_birth', $artist->day_birth) }}">
                      <label for="name">Death</label>
                      <input type="date" class="form-control" id="death" name="day_death" value="{{ old('day_death', $artist->day_death) }}">
                    </div>
                    
                    <a type="button" class="btn btn-success" href="{{ route('artists.index') }}">Back</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

