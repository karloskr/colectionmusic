@extends('layouts.main')

@section('title', 'Artists')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Artists</h1>
    <a href="{{ route('artists.create') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"}><i
            class="fas fa-user fa-sm text-white-50 Create"></i>Create</a>
</div> 


<div class="row">
    @include('custom.message')
</div>

<!-- List artists -->
<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Artists</th>
        <th scope="col">Birth</th>
        <th scope="col">Death</th>
        <th scope="col">Profile</th>
        <th scope="col">Countries</th>
        <th scope="col"></th>
        <th scope="col"></th>
      </tr>
    </thead>
    <tbody>
     @foreach ($artists as $artist)
     <tr>
        <td scope="row">{{ $artist->id }}</td>
        <td scope="row"><a href="{{ route('artists.show', $artist->id) }}">{{ $artist->name }}</a></td>
        <td scope="row">{{ $artist->day_birth }}</td>
        <td scope="row">{{ $artist->day_death }}</td>
        <td scope="row">{{ $artist->profile }}</td>
        <td scope="row">{{ $artist->country->name}}</td>
        <td scope="row"><a href="{{ route('artists.edit', $artist) }}" class="btn btn-primary">
        <i class="fa fa-edit"></i>
        </a></td>
        <td scope="row">
        <button class="btn btn-warning btnEliminar" data-id="{{ $artist->id }}" data-toggle="modal" data-target="#modalEliminar"><i class="fa fa-trash"></i></button>
          <form action="{{ route('artists.destroy', $artist) }}" id="formEli_{{ $artist->id}}" method="POST">
            @csrf
           {{-- @method('DELETE') --}}
            <input type="hidden" name="id" value="{{ $artist->id }}">
            <input type="hidden" name="_method" value="delete">
          </form>
        </td>
      </tr>
     @endforeach
    </tbody>
  </table>
  <span>
    {{ $artists->links() }}
  </span>

<!-- Modals -->
@include('artists.modalDelete')
<!-- EndModals -->

@endsection

@section('scripts')
      <script>

        var idEliminar = 0;

          $(document).ready(function(){
            @if($message = session('ErrorInsert'))
                $('#Create').modal('show');
            @endif
            $(".btnEliminar").click(function(){
                idEliminar = $(this).data('id');
            });
            $(".btnModalEliminar").click(function(){
                $("#formEli_" + idEliminar).submit();
            });

          });
      </script>
  @endsection

