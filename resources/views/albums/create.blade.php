@extends('layouts.main')

@section('title', 'Create Album')

@section('content')
    

<div class="container-sm">
  <form method="POST" action="{{route('bands.albums.store', $band)}}">
    @csrf
    <div class="form-group">
            {{-- Labels List --}}
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fa fa-map-marker-alt text-info"></i></div>
                </div>
                <select name="label_id" class="form-control">
                  <option class="hidden" selected disabled>Labels</option>
                  @foreach ($labels as $label)
                    <option value="{{ $label->id }}">
                      {{ $label->name }}</option>
                  @endforeach
              </select>
            </div>
            @error('label_id')
            <p class="help is-danger" style="color: red">{{ $message }}</p>
            @enderror
            <br>

      <label for="name">Name</label>
      <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
      @error('name')
            <p class="help is-danger" style="color: red">{{ $message }}</p>
      @enderror
  
      <label for="name">Year</label>
      <input type="number" class="form-control" id="year" name="year" value="{{ old('year') }}">
      @error('year')
            <p class="help is-danger" style="color: red">{{ $message }}</p>
      @enderror
    </div>

    <label for="myCheck">Styles:</label>
    <input type="checkbox" id="myCheck" onclick="myFunction()">

      <div class="container" id="text" style="display:none">
        <div class="row row-cols-3">
          @foreach ($styles as $style)
            <div class="col"> <input type="checkbox" name="style[]" id="style_{{ $style->id }}" value="{{ $style->id }}"
              @if (is_array(old('style')) && in_array("$style->id", old('style'))  )
                checked
            @endif
              > {{ $style->name }}</div>
          @endforeach
        </div>
      </div>
    <br>
</div>
  
  
  <div class="modal-footer">
    <a href="{{ route('bands.albums.index', $band) }}" class="float-right btn btn-success">Back</a>
    <button type="submit" class="btn btn-primary">Save changes</button>
  </div>
  </form> 
</div>
    
@endsection

@section('scripts')
  <script>
    function myFunction() {
      var checkBox = document.getElementById("myCheck");
      var text = document.getElementById("text");
      if (checkBox.checked == true){
        text.style.display = "block";
      } else {
        text.style.display = "none";
      }
    }
    </script>
@endsection
