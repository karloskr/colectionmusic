@extends('layouts.main')

@section('title', 'Show Album')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">{{ $album->bands->name}}</h1>
</div> 

<div class="card">
    <div class="card-body">
        <h4>Album</h4>
        <br>

        <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $album->name) }}">
            @error('name')
              <p class="help is-danger" style="color: red">{{ $message }}</p>
            @enderror

        <label for="name">Year</label>
            <input type="number" class="form-control" id="year" name="year" value="{{ old('year', $album->year) }}">
            @error('year')
              <p class="help is-danger" style="color: red">{{ $message }}</p>
          @enderror
    </div>
  </div>
  <br>
  <a href="{{ route('bands.albums.index', $band) }}" class="float-left btn btn-success">Back</a>
</div>
@endsection