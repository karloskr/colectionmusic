@extends('layouts.main')

@section('title', 'Edit albums')

@section('content')
<br>

<div class="form-group">
  <form method="POST" action="{{route('albums.update',$album)}}">
    @csrf
    @method('PUT')
    <h2>Edit Album</h2>
    {{-- Labels List --}}
    <label for="label">Labels</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <div class="input-group-text"><i class="fa fa-map-marker-alt text-info"></i></div>
        </div>
        <select name="label_id" class="form-control">
          @foreach ($labels as $label)
            <option value="{{ $label->id }}"
              @if ($label->id === $album->label_id)
                selected
              @endif
              >
              {{ $label->name }}
            </option>
          @endforeach
      </select>
    </div>
    <br>

    <label for="name">Name</label>
    <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $album->name) }}">
    @error('name')
      <p class="help is-danger" style="color: red">{{ $message }}</p>
    @enderror

    <label for="name">Year</label>
    <input type="number" class="form-control" id="year" name="year" value="{{ old('year', $album->year) }}">
    </div>
    @error('year')
      <p class="help is-danger" style="color: red">{{ $message }}</p>
    @enderror

    <div class="container" id="text" {{-- style="display:none" --}}>
      <div class="row row-cols-3">
        @foreach ($styles as $style)
          <div class="col"> <input type="checkbox" name="style[]"  id="style_{{ $style->id }}" 
            value="{{ $style->id }}"
            @if (is_array(old('style')) && in_array("$style->id", old('style'))  )
                checked
            @elseif (is_array($album_style) && in_array("$style->id", $album_style)  )
              checked
            @endif>
            {{ $style->name }}</div>
        @endforeach
      </div>
    </div>
  <br>
</div>
    
    <div class="modal-footer">
      <a href="{{ route('bands.albums.index', $band) }}" class="float-right btn btn-success">Back</a>
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
  
</div>
  
@endsection

