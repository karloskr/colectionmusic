<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-record-vinyl"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Collection<sup>'</sup></div>
    </a>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
            aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-cog"></i>
            <span>Inicio</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Components:</h6>
                <a class="collapse-item" href="{{ route('countries.index') }}">Countries</a>
                <a class="collapse-item" href="{{ route('labels.index') }}">Labels</a>
                <a class="collapse-item" href="{{ route('artists.index') }}">Artists</a>
                <a class="collapse-item" href="{{ route('bands.index') }}">Bands</a>
                <a class="collapse-item" href="{{ route('genders.index') }}">Genders</a>
            </div>
        </div>
    </li>
    <!-- Divider -->
   {{--  <hr class="sidebar-divider my-0">
        <li class="nav-item">
            <a class="nav-link" href="/admin/">
                <i class="fas fa-fw fa-table"></i>
                <span>Admin</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/admin/">
                <i class="fas fa-fw fa-table"></i>
                <span>Usuarios</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('genders.index') }}">
                <i class="fas fa-fw fa-table"></i>
                <span>Genders</span></a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{ route('bands.index') }}">
                <i class="fas fa-fw fa-table"></i>
                <span>Bands</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('countries.index') }}">
                <i class="fas fa-fw fa-table"></i>
                <span>Countries</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('artists.index') }}">
                <i class="fas fa-fw fa-table"></i>
                <span>Artists</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('labels.index') }}">
                <i class="fas fa-fw fa-table"></i>
                <span>Labels</span></a>
        </li> --}}

        
</ul>