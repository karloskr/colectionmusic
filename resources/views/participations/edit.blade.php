@extends('layouts.main')

@section('title', 'Update Participation')

@section('content')
<br>
<div class="container">
  <div class="row justify-content-center">
      <div class="col-md-8">
          <div class="card">
              <div class="card-header">
                  <h2>Edit Participation</h2>
              </div>
              <div class="card-body">
                <form method="POST" action="{{route('artist_tracks.update', $artist_track)}}">
                  @csrf
                  @method('PUT')
          
                  {{-- Participations --}}
                  <label for="exampleFormControlSelect1">List Participations</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <div class="input-group-text"><i class="fa fa-map-marker-alt text-info"></i></div>
                      </div>
                      <select name="participation_id" class="form-control">
                        <option class="hidden" selected disabled>Participations</option>
                        @foreach ($participations as $participation)
                          <option value="{{ $participation->id }}"
                            @if ($participation->id === $artist_track->participation_id)
                                selected
                            @endif
                            >
                            {{ $participation->name }}</option>
                        @endforeach
                    </select>
                  </div>
                  @error('participation_id')
                  <p class="help is-danger" style="color: red">{{ $message }}</p>
                  @enderror
                  <br>
                  {{-- Artists List --}}
                  <label for="exampleFormControlSelect1">List Participations</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                          <div class="input-group-text"><i class="fa fa-map-marker-alt text-info"></i></div>
                      </div>
                      <select name="artist_id" class="form-control">
                        <option class="hidden" selected disabled>Artists</option>
                        @foreach ($artists as $artist)
                          <option value="{{ $artist->id }}"
                            @if ($artist->id === $artist_track->artist_id)
                                selected
                            @endif
                            >
                            {{ $artist->name }}</option>
                        @endforeach
                    </select>
                  </div>
                  @error('artist_id')
                  <p class="help is-danger" style="color: red">{{ $message }}</p>
                  @enderror

                  <div class="modal-footer">
                    <a href="{{ route('tracks.artist_tracks.index', $track) }}" class="btn btn-success">Back</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>

                </form>
              </div>
          </div>
      </div>
  </div>
</div>
@endsection

