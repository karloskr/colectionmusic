@extends('layouts.main')

@section('title', 'Create Participations')

@section('content')

<div class="container-sm">
  <form method="POST" action="{{route('tracks.artist_tracks.store', $track)}}">
    @csrf
    <div class="form-group">
        <h3>Create participations</h3>
            <br>
        {{-- Participations --}}
          <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text"><i class="fa fa-map-marker-alt text-info"></i></div>
          </div>
          <select name="participation_id" class="form-control">
            <option class="hidden" selected disabled>Participations</option>
            @foreach ($participations as $participation)
              <option value="{{ $participation->id }}">
                {{ $participation->name }}</option>
            @endforeach
        </select>
      </div>
      @error('participation_id')
      <p class="help is-danger" style="color: red">{{ $message }}</p>
      @enderror

            {{-- Artists List --}}
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fa fa-map-marker-alt text-info"></i></div>
                </div>
                <select name="artist_id" class="form-control">
                  <option class="hidden" selected disabled>Artists</option>
                  @foreach ($artists as $artist)
                    <option value="{{ $artist->id }}">
                      {{ $artist->name }}</option>
                  @endforeach
              </select>
            </div>
            @error('artist_id')
            <p class="help is-danger" style="color: red">{{ $message }}</p>
            @enderror

            <br>

{{--   
      <label for="name">Name</label>
      <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
      @error('name')
            <p class="help is-danger" style="color: red">{{ $message }}</p>
      @enderror --}}
  
  
  <div class="modal-footer">
    <a href="{{ route('tracks.artist_tracks.index',$track) }}" class="float-right btn btn-success">Back</a>
    <button type="submit" class="btn btn-primary">Save changes</button>
  </div>
  </form>
  
</div>
    
@endsection

