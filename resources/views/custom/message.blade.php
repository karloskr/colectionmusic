
@if ($errors->any())
    <div class="col-12 alert alert-danger" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


{{-- validate error create gender --}}
@if (session('ErrorInsert'))
          <div class="col-12 alert alert-danger alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              {{ session('ErrorInsert') }}
          </div>
    @endif


{{-- GENDER --}}


{{-- validate store gender --}}  
@if (session('gender_store'))
    <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
        {{ session('gender_store') }}
    </div>
@endif

{{-- validate update gender --}}  
@if (session('gender_update'))
    <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
        {{ session('gender_update') }}
    </div>
@endif

{{-- validate delete gender --}}  
@if (session('gender_destroy'))
    <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
        {{ session('gender_destroy') }}
    </div>
@endif


{{-- STYLES --}}

{{-- alert create styles --}}
@if (session('style_store'))
          <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              {{ session('style_store') }}
          </div>
@endif

{{-- alert update styles --}}
@if (session('style_update'))
          <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              {{ session('style_update') }}
          </div>
@endif

{{-- alert delete styles --}}
@if (session('style_delete'))
          <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              {{ session('style_delete') }}
          </div>
@endif


{{-- LABELS --}}

{{-- alert store labels --}}
@if (session('label_store'))
          <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              {{ session('label_store') }}
          </div>
@endif

{{-- alert update labels --}}
@if (session('label_update'))
          <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              {{ session('label_update') }}
          </div>
@endif

{{-- alert delete labels --}}
@if (session('label_destroy'))
          <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              {{ session('label_destroy') }}
          </div>
@endif


{{-- ALBUM --}}

{{-- alert store album --}}
@if (session('album_store'))
          <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              {{ session('album_store') }}
          </div>
@endif

{{-- alert update album --}}
@if (session('album_update'))
          <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              {{ session('album_update') }}
          </div>
@endif

{{-- alert delete album --}}
@if (session('album_destroy'))
          <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              {{ session('album_destroy') }}
          </div>
@endif




{{-- PARTICIPATION --}}


{{-- alert store participations --}}
@if (session('participation_store'))
          <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              {{ session('participation_store') }}
          </div>
@endif

{{-- alert update participations --}}
@if (session('participation_update'))
          <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              {{ session('participation_update') }}
          </div>
@endif

{{-- alert delete participations --}}
@if (session('participation_destroy'))
          <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              {{ session('participation_destroy') }}
          </div>
@endif

{{-- ARTIST --}}
{{-- alert store artists --}}
@if (session('artist_store'))
          <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              {{ session('artist_store') }}
          </div>
@endif

{{-- alert update artists --}}
@if (session('artist_update'))
          <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              {{ session('artist_update') }}
          </div>
@endif

{{-- alert delete artists --}}
@if (session('artist_update'))
          <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              {{ session('artist_update') }}
          </div>
@endif


{{-- MEMBERS --}}

{{-- alert store members --}}
@if (session('member_store'))
          <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              {{ session('member_store') }}
          </div>
@endif

{{-- alert update members --}}
@if (session('member_update'))
          <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              {{ session('member_update') }}
          </div>
@endif

{{-- alert delete members --}}
@if (session('member_destroy'))
          <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              {{ session('member_destroy') }}
          </div>
@endif

{{-- Tracks --}}

{{-- alert store members --}}
@if (session('track_store'))
          <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              {{ session('track_store') }}
          </div>
@endif

{{-- alert update members --}}
@if (session('track_update'))
          <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              {{ session('track_update') }}
          </div>
@endif

{{-- alert delete members --}}
@if (session('track_destroy'))
          <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              {{ session('track_destroy') }}
          </div>
@endif


{{-- BANDS --}}

{{-- alert store bands --}}
@if (session('band_store'))
          <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              {{ session('band_store') }}
          </div>
@endif

{{-- alert update bands --}}
@if (session('band_update'))
          <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              {{ session('band_update') }}
          </div>
@endif

{{-- alert delete bands --}}
@if (session('band_destroy'))
          <div class="col-12 alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
              {{ session('band_destroy') }}
          </div>
@endif