@extends('layouts.main')

@section('title', 'Create Member')

@section('content')

<div class="card">
    <h5 class="card-header">Create Member</h5>
    <div class="card-body">
        <form method="POST" action="{{route('bands.members.store', $band)}}">
            @csrf
            <div class="form-group">
              {{-- Artists --}}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fa fa-microphone-alt text-info"></i></div>
                        </div>
                        <select name="artist_id" class="form-control">
                          <option class="hidden" selected disabled>Artists</option>
                          @foreach ($artists as $artist)
                            <option value="{{ $artist->id }}">
                              {{ $artist->name }}</option>
                          @endforeach
                      </select>
                    </div>
                    @error('artist_id')
                    <p class="help is-danger" style="color: red">{{ $message }}</p>
                    @enderror
                    <br>
                    {{-- Bands --}}
                    <div class="input-group">
                      <div class="input-group-prepend">
                          <div class="input-group-text"><i class="fa fa-microphone-alt text-info"></i></div>
                      </div>
                      <select name="band_id" class="form-control">
                        <option class="hidden" selected disabled>Bands</option>
                        @foreach ($bands as $band)
                          <option value="{{ $band->id }}">
                            {{ $band->name }}</option>
                        @endforeach
                    </select>
                  </div>
                  @error('band_id')
                  <p class="help is-danger" style="color: red">{{ $message }}</p>
                  @enderror

              <label for="name">Inicio</label>
              <input type="number" class="form-control" id="init" name="init">
              @error('init')
              <p class="help is-danger" style="color: red">{{ $message }}</p>
              @enderror
              <br>
              <label for="name">End</label>
              <input type="number" class="form-control" id="end" name="end">
              @error('end')
              <p class="help is-danger" style="color: red">{{ $message }}</p>
              @enderror       

            </div>
          
          <div class="modal-footer">
            <a type="button" class="btn btn-success" href="{{ route('bands.members.index', $band) }}">Back</a>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
    </div>
  </div>
    
@endsection