@extends('layouts.main')

@section('title', 'Update Period')

@section('content')
<br>
  <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h2>Edit Members</h2>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{route('members.update',$member)}}">
                    @csrf
                    @method('PUT')
                    <label for="init">Artists</label>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fa fa-microphone-alt text-info"></i></div>
                            </div>
                            <select name="artist_id" class="form-control">
                              <option class="hidden" selected disabled>Artists</option>
                              @foreach ($artists as $artist)
                                <option value="{{ $artist->id }}"
                                    @if ($artist->id === $member->artist_id)
                                      selected
                                    @endif>
                                    {{ $artist->name }}
                                </option>
                              @endforeach
                          </select>
                        </div>
                        @error('artist_id')
                        <p class="help is-danger" style="color: red">{{ $message }}</p>
                        @enderror
                  <br>
                  {{-- Bands --}}
                  <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fa fa-microphone-alt text-info"></i></div>
                    </div>
                    <select name="band_id" class="form-control">
                      <option class="hidden" selected disabled>Bands</option>
                      @foreach ($bands as $band)
                        <option value="{{ $band->id }}"
                          @if ($band->id === $member->band_id)
                              selected
                          @endif
                          >
                          {{ $band->name }}</option>
                      @endforeach
                  </select>
                </div>
                @error('band_id')
                <p class="help is-danger" style="color: red">{{ $message }}</p>
                @enderror
                <br>

                  <label for="init">Start</label>
                  <input type="number" class="form-control" id="init" name="init" value="{{ old('init', $member->init) }}">
                  <br>
                  @error('init')
                        <p class="help is-danger" style="color: red">{{ $message }}</p>
                  @enderror

                  <label for="end">End</label>
                  <input type="number" class="form-control" id="end" name="end" value="{{ old('end', $member->end) }}">
                  @error('end')
                        <p class="help is-danger" style="color: red">{{ $message }}</p>
                  @enderror
                </div>
                    
                  <div class="model-footer">
                    <a type="button" class="btn btn-success" href="{{ route('bands.members.index', $band) }}">Back</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
