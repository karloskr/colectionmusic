@extends('layouts.main')

@section('title', 'Members')

@section('content')


<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Members</h1>
    <a href="{{ route('bands.members.create', $band) }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
            class="fas fa-user fa-sm text-white-50 Create"></i>Create</a>
</div> 


<div class="row">
    @include('custom.message')
</div>

<!-- List members -->
<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Artist</th>
        <th scope="col">Bands</th>
        <th scope="col">Inicio</th>
        <th scope="col">End</th>
        <th scope="col"></th>
      </tr>
    </thead>
    <tbody>
     @foreach ($members as $member)
     <tr>
        <td scope="row">{{ $member->id }}</td>
        <td scope="row">{{ $member->relArtist->name}}</td>
        <td scope="row">{{ $member->relBand->name}}</td>
        <td scope="row">{{ $member->init }}</td>
        <td scope="row">{{ $member->end }}</td>
        <td scope="row"><a href="{{ route('members.edit', $member) }}" class="btn btn-primary">
        <i class="fa fa-edit"></i>
        </a></td>
        <td scope="row">
        <button class="btn btn-warning btnEliminar" data-id="{{ $member->id }}" data-toggle="modal" data-target="#modalEliminar"><i class="fa fa-trash"></i></button>
          <form action="{{ route('members.destroy', $member) }}" id="formEli_{{ $member->id}}" method="POST">
            @csrf
           {{-- @method('DELETE') --}}
            <input type="hidden" name="id" value="{{ $member->id }}">
            <input type="hidden" name="_method" value="delete">
            
          </form>
        </td>
      </tr>
     @endforeach
    </tbody>
  </table>
  <span>
    {{ $members->links() }}
  </span>

    <a href="{{ route('bands.index',$band) }}" class="btn btn-success shadow-sm" id="back">Back</a>


<!-- Modals -->
@include('members.modalDelete')
<!-- EndModals -->

@endsection

@section('scripts')
      <script>

        var idEliminar = 0;

          $(document).ready(function(){
            /* @if($message = session('ErrorInsert'))
                $('#Create').modal('show');
            @endif */
            $(".btnEliminar").click(function(){
                idEliminar = $(this).data('id');
            });
            $(".btnModalEliminar").click(function(){
                $("#formEli_" + idEliminar).submit();
            });

          });
      </script>
  @endsection
