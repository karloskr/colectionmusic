@extends('layouts.main')

@section('title', 'List Tracks')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Tracks</h1>
    <a href="{{ route('albums.tracks.create', $album) }}" class="btn btn-primary">Create</a>
</div> 

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('bands.albums.index', $band) }}">Albums</a></li>
    <li class="breadcrumb-item" aria-current="page">Tracks</li>
    <li class="breadcrumb-item active" aria-current="page">{{ $album->name}}</li>
  </ol>
</nav>

<div class="row">
    @include('custom.message')
</div>

<!-- List Tracks -->
<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Tracks</th>
        <th scope="col">Duration</th>
        <th scope="col">Style</th>
        <th scope="col">Participation</th>
        <th scope="col"></th>
        <th scope="col"></th>
      </tr>
    </thead>
    <tbody>
     @foreach ($tracks as $track)
     <tr>
        <td scope="row">{{ $track->id }}</td>
        <td scope="row"><a href="{{ route('tracks.show', $track->id) }}">{{ $track->name }}</a></td>
        <td scope="row">{{ $track->duration }}</td>
        <td scope="row">{{ $track->style->name }}</td>
        <td scope="row"><a href="{{ route('tracks.artist_tracks.index', $track) }}">{{ $track->rel_artists_tracks_count }}</a></td>
        <td scope="row"><a href="{{ route('tracks.edit',$track) }}" class="btn btn-primary">
        <i class="fa fa-edit"></i>
        </a></td>
        <td scope="row">
        <button class="btn btn-warning btnEliminar" data-id="{{ $track->id }}" data-toggle="modal" data-target="#modalEliminar"><i class="fa fa-trash"></i></button>
          <form action="{{ route('tracks.destroy', $track) }}" id="formEli_{{ $track->id}}" method="POST">
            @csrf
            <input type="hidden" name="id" value="{{ $track->id }}">
            <input type="hidden" name="_method" value="delete">
            
          </form>
        </td>
      </tr>
     @endforeach
    </tbody>
  </table>
  <span>
    {{ $tracks->links() }}
  </span>

<div class="modal-footer">
  <a href="{{ route('bands.albums.index', $band) }}" class="float-left btn btn-success">Back</a>
</div>
<!-- Modals -->
{{-- @include('Tracks.modalCreate') --}}
@include('Tracks.modalDelete')
<!-- EndModals -->

@endsection

@section('scripts')
      <script>

        var idEliminar = 0;

          $(document).ready(function(){
            $(".btnEliminar").click(function(){
                idEliminar = $(this).data('id');
            });
            $(".btnModalEliminar").click(function(){
                $("#formEli_" + idEliminar).submit();
            });

          });
      </script>
  @endsection

