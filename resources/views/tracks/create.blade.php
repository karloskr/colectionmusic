@extends('layouts.main')

@section('title', 'Create Tracks')

@section('content')

<div class="container-sm">
  <form method="POST" action="{{route('albums.tracks.store', $album)}}">
    @csrf
    <div class="form-group">
        <h3>Create Tracks</h3>
            <br>
            {{-- Styles List --}}
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fa fa-map-marker-alt text-info"></i></div>
                </div>
                <select name="style_id" class="form-control">
                  <option class="hidden" selected disabled>Styles</option>
                  @foreach ($styles as $style)
                    <option value="{{ $style->id }}">
                      {{ $style->name }}</option>
                  @endforeach
              </select>
            </div>
            @error('style_id')
            <p class="help is-danger" style="color: red">{{ $message }}</p>
            @enderror
  
      <label for="name">Name</label>
      <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
      @error('name')
            <p class="help is-danger" style="color: red">{{ $message }}</p>
      @enderror
  
      <label for="name">Duration</label>
      <input type="number" class="form-control" id="duration" name="duration" value="{{ old('duration') }}">
      @error('duration')
            <p class="help is-danger" style="color: red">{{ $message }}</p>
      @enderror
    </div>
  
  <div class="modal-footer">
    <a href="{{ route('albums.tracks.index', $album) }}" class="float-right btn btn-success">Back</a>
    <button type="submit" class="btn btn-primary">Save changes</button>
  </div>
  </form>
  
</div>
    
@endsection

