@extends('layouts.main')

@section('title', 'Edit Tracks')

@section('content')
<br>

<div class="form-group">
  <form method="POST" action="{{route('tracks.update',$track)}}">
    @csrf
    @method('PUT')
    <h2>Edit Track</h2>
    {{-- Labels List --}}
    <label for="label">Styles</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <div class="input-group-text"><i class="fa fa-map-marker-alt text-info"></i></div>
        </div>
        <select name="style_id" class="form-control">
          @foreach ($styles as $style)
            <option value="{{ $style->id }}"
              @if ($style->id === $track->style->id)
                selected
              @endif
              >
              {{ $style->name }}
            </option>
          @endforeach
      </select>
    </div>
    <br>

    <label for="name">Name</label>
    <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $track->name) }}">
    @error('name')
      <p class="help is-danger" style="color: red">{{ $message }}</p>
    @enderror

    <label for="name">Duration</label>
    <input type="number" class="form-control" id="duration" name="duration" value="{{ old('year', $track->duration) }}">
    </div>
    @error('duration')
      <p class="help is-danger" style="color: red">{{ $message }}</p>
    @enderror
    
    <div class="modal-footer">
      <a href="{{ route('albums.tracks.index', $album) }}" class="float-right btn btn-success">Back</a>
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
  
</div>
  
@endsection

