@extends('layouts.main')

@section('title', 'Update Styles')

@section('content')
<br>
  <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h2>Edit Styles</h2>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{route('styles.update', $style)}}">
                    @csrf
                    @method('PUT')
            
                    <div class="form-group">
                      <label for="name">Name Gender</label>
                      <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $style->name )}}">
                      <span style="color: red">@error('name'){{$message}}@enderror</span>
                    </div>
                    
                    <div class="modal-footer">
                      <a type="button" class="btn btn-success" href="{{ route('genders.styles.index', $gender) }}">Back</a>
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

