@extends('layouts.main')

@section('title', 'Show Label')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Labels</h1>

</div> 

<div class="card">
    <h5 class="card-header">Show Label</h5>
    <div class="card-body">
      <h3 class="card-text">{{ $label->name }}</h3>
    </div>
  </div>
  <br>
  <a href="{{ route('labels.index') }}" class="float-left btn btn-success">Back</a>
    
@endsection