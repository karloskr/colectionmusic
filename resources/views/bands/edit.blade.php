@extends('layouts.main')

@section('title', 'Update Bands')

@section('content')
<br>
  <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h2>Edit Bands</h2>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{route('bands.update', $band)}}">
                    @csrf
                    @method('PUT')
            
                    <div class="form-group">
                    <label for="exampleFormControlSelect1">List Countries</label>
                    <select class="form-control" id="countries" name="country_id">
                        <option class="hidden" disabled>List Countries</option>
                        @foreach ($countries as $country)
                          <option value="{{ $country->id }}"
                            @if ($country->id === $band->country_id)
                                selected
                            @endif
                            >
                            {{ $country->name }}</option>
                        @endforeach
                    </select>
                    @error('country_id')
                    <p class="help is-danger" style="color: red">{{ $message }}</p>
                    @enderror
                      <label for="name">Name Band</label>
                      <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $band->name )}}">
                      @error('name')
                    <p class="help is-danger" style="color: red">{{ $message }}</p>
                    @enderror
                    </div>
                    
                    <a type="button" class="btn btn-success" href="{{ route('bands.index') }}">Back</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

