@extends('layouts.main')

@section('title', 'Bands')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Bands</h1>
    <a href="{{ route('bands.create') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
            class="fas fa-user fa-sm text-white-50 Create"></i>Create</a>
</div> 


<div class="row">
    @include('custom.message')
</div>

<!-- List bands -->
<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Bands</th>
        <th scope="col">Albums</th>
        <th scope="col">Countries</th>
        <th scope="col">Members</th>
        <th scope="col"></th>
      </tr>
    </thead>
    <tbody>
     @foreach ($bands as $band)
     <tr>
        <td scope="row">{{ $band->id }}</td>
        <td scope="row"><a href="{{ route('bands.show', $band->id) }}">{{ $band->name }}</a></td>
        <td scope="row">
          <a href="{{ route('bands.albums.index', $band)}}">{{ $band->albums_count}}</a>
        </td>
        <td scope="row">{{ $band->country->name}}</td>
        <td scope="row"><a href="{{ route('bands.members.index', $band) }}">{{ $band->members_count}}</a></td>
        <td scope="row"><a href="{{ route('bands.edit', $band) }}" class="btn btn-primary">
        <i class="fa fa-edit"></i>
        </a></td>
        <td scope="row">
        <button class="btn btn-warning btnEliminar" data-id="{{ $band->id }}" data-toggle="modal" data-target="#modalEliminar"><i class="fa fa-trash"></i></button>
          <form action="{{ route('bands.destroy', $band) }}" id="formEli_{{ $band->id}}" method="POST">
            @csrf
           {{-- @method('DELETE') --}}
            <input type="hidden" name="id" value="{{ $band->id }}">
            <input type="hidden" name="_method" value="delete">
            
          </form>
        </td>
      </tr>
     @endforeach
    </tbody>
  </table>
  <span>
    {{ $bands->links() }}
  </span>

<!-- Modals -->
{{-- @include('bands.modalCreate') --}}
@include('bands.modalDelete')
<!-- EndModals -->

@endsection

@section('scripts')
      <script>

        var idEliminar = 0;

          $(document).ready(function(){
            /* @if($message = session('ErrorInsert'))
                $('#Create').modal('show');
            @endif */
            $(".btnEliminar").click(function(){
                idEliminar = $(this).data('id');
            });
            $(".btnModalEliminar").click(function(){
                $("#formEli_" + idEliminar).submit();
            });

          });
      </script>
  @endsection

