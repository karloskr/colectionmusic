@extends('layouts.main')

@section('title', 'Create Band')

@section('content')

<div class="card">
    <h5 class="card-header">Create Band</h5>
    <div class="card-body">
        <form method="POST" action="{{route('bands.store')}}">
            @csrf
            <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fa fa-map-marker-alt text-info"></i></div>
                        </div>
                        <select name="country_id" class="form-control">
                          <option class="hidden" selected disabled>Countries</option>
                          @foreach ($countries as $country)
                            <option value="{{ $country->id }}">
                              {{ $country->name }}</option>
                          @endforeach
                      </select>
                    </div>
                    @error('country_id')
                    <p class="help is-danger" style="color: red">{{ $message }}</p>
                    @enderror

              <label for="name">Name</label>
              <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
              @error('name')
              <p class="help is-danger" style="color: red">{{ $message }}</p>
              @enderror
            </div>
          
          <div class="modal-footer">
            <a type="button" class="btn btn-success" href="{{ route('bands.index') }}">Back</a>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
    </div>
  </div>
    
@endsection

{{-- @section('scripts')    

<script>
$(document).ready(function(){

  $('#country_name').keyup(function(){ 
        var query = $(this).val();
        if(query != '')
        {
          var _token = $('input[name="_token"]').val();
          $.ajax({
          url:"{{ route('search.band') }}",
          method:"POST",
          data:{query:query, _token:_token},
          success:function(data){
            $('#countryList').fadeIn();  
            $('#countryList').html(data);

          }
    });

    $(document).on('click', 'li', function(){  
        $('#country_name').val($(this).text());  
        $('#countryList').fadeOut();
    });



  });
</script>
@endsection --}}