<?php

namespace Database\Seeders;

use App\Models\Band;
use App\Models\Country;
use Illuminate\Database\Seeder;

class BandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Band::factory(10)->create();

       /*  foreach (Band::all() as $band ){
            $countries = Country::inRandomOrder()->take(rand(1,3))->pluck('id');
            $band->tracks()->attach($countries);
        }  */
    }
}
