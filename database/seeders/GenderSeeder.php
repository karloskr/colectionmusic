<?php

namespace Database\Seeders;

use App\Models\Gender;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class GenderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* Gender::factory(10)->create(); */
        $genders = [
            ['name' => 'Electronic'],
            ['name' => 'Reggae'],
            ['name' => 'Non-Music'],
            ['name' => 'Children'],
            ['name' => 'Hip Hop'],
            ['name' => 'Latin'],
            ['name' => 'Pop'],
            ['name' => 'Folk, World, & Country'],
            ['name' => 'Jazz'],
            ['name' => 'Funk / Soul'],
            ['name' => 'Classical'],
            ['name' => 'Stage & Screen'],
            ['name' => 'Rock'],
            ['name' => 'Blues'],
            ['name' => 'Brass & Military'],
        ];

        foreach ($genders as $gender) {
            Gender::create($gender);
        }
    }
}
