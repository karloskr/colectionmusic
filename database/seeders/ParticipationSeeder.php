<?php

namespace Database\Seeders;

use App\Models\Participation;
use App\Models\Track;
use Illuminate\Database\Seeder;

class ParticipationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* Participation::factory(10)->create(); */

        $participations = array("Vocal", "Guitarrist", "Background Vocal", "Keyboards", "Bass", "Drums");

        foreach ($participations as $participation){
            Participation::create([
                'name' => $participation
              ]);
        }

    }
}
