<?php

namespace Database\Factories;

use App\Models\Album;
use App\Models\Band;
use App\Models\Label;
use Illuminate\Database\Eloquent\Factories\Factory;

class AlbumFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Album::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'label_id' => Label::inRandomOrder()->first()->id,
            'band_id' => Band::inRandomOrder()->first()->id,
            'name' => $this->faker->name,
            'year' => $this->faker->year,
        ];
    }
}
