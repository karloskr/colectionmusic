<?php

namespace Database\Factories;

use App\Models\Album;
use App\Models\Style;
use App\Models\Track;
use Illuminate\Database\Eloquent\Factories\Factory;

class TrackFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Track::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'album_id' => Album::inRandomOrder()->first()->id,
            'style_id' => Style::inRandomOrder()->first()->id,
            'name' => $this->faker->name,
            'duration' => $this->faker->numberBetween($min = 0, $max = 14),
        ];
    }
}
