<?php

namespace Database\Factories;

use App\Models\Artist;
use App\Models\Country;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArtistFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Artist::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'country_id' => Country::inRandomOrder()->first()->id,
            'name' => $this->faker->name,
            'profile' => $this->faker->text,
            'day_birth' => $this->faker->date,
            'day_death' => $this->faker->date,
        ];
    }
}
