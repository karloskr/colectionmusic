<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\AlbumStyle;
use App\Models\Band;
use App\Models\Label;
use App\Models\Style;
use Illuminate\Http\Request;

class AlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Band $band)
    {
        $albums = $band->albums()->withCount('tracks')->orderBy('id', 'desc')->paginate(5);
        
        return view('albums.index', compact('albums', 'band'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Band $band)
    {   
        $labels = Label::all();
        $styles = Style::all();
        return view('albums.create', compact('band','labels','styles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Band $band)
    {
        /* dd($request->all()); */
        $request->validate([
            'label_id' =>  'required|integer',
            'name' =>   'required|max:50',
            'year' =>   'required|digits:4|integer|min:1900|max:'.(date('Y')+1),
        ]);


        $album = $band->albums()->create($request->all());
        
        $album->styles()->attach($request->style);      

        return redirect()->route('bands.albums.index', compact('band'))->with('album_store', 'Album Created');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function show(Album $album)
    {   
        $band = $album->bands;

        return view('albums.show', compact('album','band'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function edit(Album $album)
    {   
        $labels = Label::all();
        $styles = Style::all();
        $band = $album->bands;

        $album_style=[];

        foreach($album->styles as $style){
            $album_style[] = $style->id;
        }
        return view('albums.edit', compact('album', 'labels', 'band', 'styles','album_style'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Album $album)
    {
       /*  dd($request->all()); */
        $request->validate([
            'label_id' =>   'required|integer',
            'name' =>   'required|max:50',
            'year' =>   'required|digits:4|integer|min:1900|max:'.(date('Y')+1),
        ]);

        $album->update($request->all());
        
        $album->styles()->sync($request->style);     

        $band = $album->bands;
        return redirect()->route('bands.albums.index',compact('band'))->with('album_update', 'Album Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function destroy(Album $album)
    {
        if($album->bands()->count()){
            return redirect()->back()->with('album_destroy', 'Record associated with foreign key');
        }

        $album->delete();
        return redirect()->back()->with('album_destroy','Album Deleted');
    }


}
