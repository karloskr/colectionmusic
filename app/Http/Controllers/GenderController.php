<?php

namespace App\Http\Controllers;

use App\Models\Gender;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $genders = Gender::withCount('styles')->orderBy('id', 'desc')->paginate(5);
        
        return view('genders.index', compact('genders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('genders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50|unique:genders,name',
        ]);

        if($validator->fails()){
            return back()
            ->withErrors($validator)
            ->with('ErrorInsert', 'Field Required')
            ->withInput();
        }

       Gender::create($request->all());

       return redirect()->route('genders.index')->with('gender_store','Gender Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Gender  $gender
     * @return \Illuminate\Http\Response
     */
    public function show(Gender $gender)
    {         
        return view('genders.show', compact('gender'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Gender  $gender
     * @return \Illuminate\Http\Response
     */
    public function edit(Gender $gender)
    {   
        return view('genders.edit', compact('gender'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Gender  $gender
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gender $gender)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50|unique:genders,name',
        ]);

        if($validator->fails()){
            return back()
            ->withErrors($validator)
            ->with('ErrorInsert', 'Field Required')
            ->withInput();
        }

        $gender->update($request->all());
        return redirect()->route('genders.index', $gender->id)->with('gender_update', 'Gender updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Gender  $gender
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gender $gender)
    {                                   
       if($gender->styles()->count()){
        return back()->with('gender_destroy','Record associated with foreign key');
       }
        $gender->delete();
        return back()->with('gender_destroy','Gender Deleted');

    }

    
}
