<?php

namespace App\Http\Controllers;

use App\Models\Label;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LabelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $labels = Label::orderBy('id', 'Desc')->paginate(5);

        return view('labels.index', compact('labels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('labels.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50|unique:labels,name',
        ]);

        if($validator->fails()){
            return back()
            ->withErrors($validator)
            ->with('ErrorInsert', 'Field Required')
            ->withInput();
        }

        Label::create($request->all());
        return redirect()->route('labels.index')->with('label_store','Label Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Label  $label
     * @return \Illuminate\Http\Response
     */
    public function show(Label $label)
    {
        return view('labels.show', compact('label'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Label  $label
     * @return \Illuminate\Http\Response
     */
    public function edit(Label $label)
    {
        return view('labels.edit', compact('label'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Label  $label
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Label $label)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50|unique:labels,name',
        ]);

        if($validator->fails()){
            return back()
            ->withErrors($validator)
            ->with('ErrorInsert', 'Field Required')
            ->withInput();
        }
        $label->update($request->all());

        return redirect()->route('labels.index', $label->id)->with('label_update', 'Label updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Label  $label
     * @return \Illuminate\Http\Response
     */
    public function destroy(Label $label)
    {
        if($label->albums()->count()){
            return redirect()->back()->with('label_destroy','Record associated with foreign key');
        }
        $label->delete();
        return redirect()->route('labels.index')->with('label_destroy','Label Deleted');
    }
}
