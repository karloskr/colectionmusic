<?php

namespace App\Http\Controllers;

use App\Models\Band;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class BandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bands = Band::with('country')
        ->withCount('members','albums')
        ->orderBy('id', 'Desc')->paginate(5);
        
        return view('bands.index', compact('bands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $countries = Country::all();
        return view('bands.create',compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'country_id' =>    'required|integer',
            'name' =>   'required|max:50',
        ]);

        Band::create($request->all());   
    
        return redirect()->route('bands.index')->with('band_store', 'Bands Created');;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Band  $band
     * @return \Illuminate\Http\Response
     */
    public function show(Band $band)
    {   
        $countries = Country::all();

        return view('bands.show', compact('band', 'countries'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Band  $band
     * @return \Illuminate\Http\Response
     */
    public function edit(Band $band)
    {
        $countries = Country::all();
        return view('bands.edit',compact('band','countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Band  $band
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Band $band)
    {
        $request->validate([
            'country_id' =>    'required|integer',
            'name' =>   'required|max:50',
        ]);

        $band->update($request->all());

        return redirect()->route('bands.index', $band)->with('band_update', 'Band updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Band  $band
     * @return \Illuminate\Http\Response
     */
    public function destroy(Band $band)
    {
        if($band->albums()->count()){
            return back()->with('band_destroy', 'Record associated with foreign key');
        }
        $band->delete();
        return back()->with('band_destroy','Band Deleted');
    }

    /* public function fetch(Request $request)
    {
     if($request->get('query'))
     {
      $query = $request->get('query');

      $data = DB::table('countries')
        ->where('name', 'LIKE', "%{$query}%")
        ->get();
      $output = '<ul class="dropdown-menu" style="display:block; position:relative">';

      foreach($data as $row)
      {
       $output .= '
       <li><a href="#">'.$row->name.'</a></li>
       ';
      }

      $output .= '</ul>';
      echo $output;

     }

     
    } */
}
