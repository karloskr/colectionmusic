<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\Band;
use App\Models\Participation;
use App\Models\Style;
use App\Models\Track;
use Illuminate\Http\Request;

class TrackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Album $album)
    {
        $tracks = $album->tracks()
        ->withCount('relArtistsTracks')->orderBy('id', 'desc')->paginate(5);
 
        $band = $album->bands;

        return view('tracks.index ', compact('tracks','album', 'band'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Album $album)
    {
        $styles = Style::all();
        return view('tracks.create', compact('styles','album'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Album $album)
    {
        $request->validate([
            'style_id' =>  'required|integer',
            'name' =>   'required|max:50',
            'duration' =>   'required|integer',
        ]);
        
        $album->tracks()->create($request->all());

        return redirect()->route('albums.tracks.index',compact('album'))->with('track_store','Track Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Track  $track
     * @return \Illuminate\Http\Response
     */
    public function show(Track $track)
    {
        $style = $track->style;
        $album = $track->albums;   
        return view('tracks.show', compact('style', 'track', 'album'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Track  $track
     * @return \Illuminate\Http\Response
     */
    public function edit(Track $track)
    {   
        $styles = Style::all();
        $album = $track->albums;
        return view('tracks.edit', compact('track','styles', 'album'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Track  $track
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Track $track)
    {
        $request->validate([
            'style_id' =>  'required|integer',
            'name' =>   'required|max:50',
            'duration' =>   'required|integer',
        ]);

        $track->update($request->all());
        $album = $track->albums;

        return redirect()->route('albums.tracks.index', compact('track', 'album'))->with('track_update','Track Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Track  $track
     * @return \Illuminate\Http\Response
     */
    public function destroy(Track $track)
    {
        if($track->albums()->count()){
            return redirect()->back()->with('track_destroy','Record associated with foreign key');  
        }
        $track->delete();
        return redirect()->back()->with('track_destroy','Track Deleted');
    }
}
