<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\Country;
use Illuminate\Http\Request;


class ArtistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $artists = Artist::with('country')
        ->orderBy('id','desc')->paginate(5);

        return view('artists.index',compact('artists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        return view('artists.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'country_id' =>   'required|integer',
            'name' =>   'required|max:50|unique:artists,name',
            'profile' =>   'required|max:500|',
            'day_birth' =>   'nullable|date|',
            'day_death' =>   'nullable|date|',

        ]);
        Artist::create($request->all());

        return redirect()->route('artists.index')->with('artist_store', 'Artist Created');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function show(Artist $artist)
    { 
        $countries = Country::all();
        return view('artists.show', compact('artist','countries'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function edit(Artist $artist)
    {
        $countries = Country::all();

        return view('artists.edit', compact('artist', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Artist $artist)
    {
        $request->validate([
            'country_id' =>   'required|integer',
            'name' =>   'required|max:50|unique:artists,name',
            'profile' =>   'required|max:500|',
            'day_birth' =>   'nullable|date|',
            'day_death' =>   'nullable|date|',

        ]);

        return redirect()->route('artists.index', $artist)->with('artist_update', 'Artist Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Artist $artist)
    {
        $artist->delete();
        return back()->with('artist_destroy','Artists Deleted');
    }

}
