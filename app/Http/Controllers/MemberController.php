<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\Band;
use App\Models\Member;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Band $band)
    {
        /* $members = Member::orderBy('id', 'Desc')->paginate(); */
        
        $members = $band->members()
        ->with('relArtist')
        ->orderBy('id', 'Desc')->paginate(5); 
        return view('members.index', compact('members', 'band'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Band $band)
    {
        $artists = Artist::all();
        $bands = Band::all();
        return view('members.create', compact('band', 'artists', 'bands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Band $band)
    {
        $request->validate([
            'artist_id' =>    'required|integer',
            'band_id' =>    'required|integer',
            'init' =>   'nullable|digits:4|integer|min:1900|max:'.(date('Y')+1),
            'end' =>   'nullable|digits:4|integer|min:1900|max:'.(date('Y')+1),
        ]);

        $members = $band->members()->create($request->all());

        return redirect()->route('bands.members.index', compact('band', 'members'))->with('member_store','Member Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function show(Member $member)
    {
        /* return view('members.index', compact('member')); */
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function edit(Member $member)
    {
        $artists = Artist::all();
        $bands = Band::all();
        $band = $member->relBand;  
        return view('members.edit', compact('member','artists','band', 'bands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Member $member)
    {
        $request->validate([
            'artist_id' =>    'required|integer',
            'band_id' =>    'required|integer',
            'init' =>   'required|digits:4|integer|min:1900|max:'.(date('Y')+1),
            'end' =>   'nullable|digits:4|integer|min:1900|max:'.(date('Y')+1),
        ]);

        $member->update($request->all());

        $band = $member->relArtist;

        return redirect()->route('bands.members.index', compact('member', 'band'))->with('member_update','Member Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function destroy(Member $member)
    {
        $member->delete();

        return back()->with('member_destroy','Member Deleted');
    }

}
