<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Style;

class Gender extends Model
{
    use HasFactory;

    protected $table = 'genders';

    protected $fillable = ['name'];

    public function styles()
    {
        return $this->hasMany(Style::class, 'gender_id');
    }

}
