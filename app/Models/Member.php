<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    use HasFactory;

    protected $table = 'members';

    protected $fillable = ['init','end','artist_id', 'band_id'];

    # RELATIONSHIPS

    public function relArtist()
    {
        return $this->belongsTo(Artist::class,'artist_id');
    }

    public function relBand()
    {
        return $this->belongsTo(Band::class, 'band_id');
    }

}
