<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Style extends Model
{
    use HasFactory;

    protected $table = 'styles';

    protected $fillable = ['name', 'gender_id'];

    public function albums()
    {
        return $this->belongsToMany(Album::class, 'album_style', 'album_id', 'style_id')->withTimestamps();
    }

    public function gender()
    {
        return $this->belongsTo(Gender::class, 'gender_id');
    }

    public function tracks()
    {
        return $this->hasMany(Track::class, 'track_id');
    }

    public function albumstyle()
    {
        return $this->hasMany(AlbumStyle::class, 'style_id');
    }
}
