<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AlbumStyle extends Model
{
    use HasFactory;

    protected $table = 'album_style';

    protected $fillable = ['album_id', 'style_id'];

    # RELATIONSHIPS

    public function relAlbums()
    {
        return $this->belongsTo(Album::class,'album_id');
    }

    public function relStyle()
    {
        return $this->belongsTo(Style::class, 'style_id');
    }
}
