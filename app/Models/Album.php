<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;



class Album extends Model
{
    use HasFactory;

    protected $table = 'albums';

    protected $fillable = ['name','year','label_id','band_id'];

    
    public function styles()
    {
        return $this->belongsToMany(Style::class, 'album_style', 'album_id', 'style_id')->withTimestamps();
    }

    public function labels()
    {
        return $this->belongsTo(Label::class,'label_id');
    }

    public function tracks()
    {
        return $this->hasMany(Track::class, 'album_id');
    }

    public function bands()
    {
        return $this->belongsTo(Band::class, 'band_id');
    }

    public function albumstyle()
    {
        return $this->hasMany(AlbumStyle::class, 'album_id');
    }

}
