<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    use HasFactory;

    protected $table = 'artists';

    protected $fillable = ['name', 'day_birth', 'day_death', 'profile','country_id'];

    public function tracks()
    {
        return $this->belongsToMany(Track::class)->withTimestamps();
    }

    public function bands()
    {
        return $this->belongsToMany(Band::class, 'members', 'artist_id', 'band_id')->withTimestamps();
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function members()
    {
        return $this->hasMany(Member::class, 'artist_id');
    }

    public function relArtistsTracks()
    {
        return $this->hasMany(ArtistTrack::class, 'artist_id');
    }



}
